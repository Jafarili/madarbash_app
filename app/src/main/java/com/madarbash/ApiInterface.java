package com.madarbash;

/**
 * Created by ali on 4/22/2017.
 */

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET("posts")
    Call<JsonArray> post_get_list(@Query("page") int page, @Query("per_page") int per_page);

    @GET("posts/{id}")
    Call<JsonObject> post_get(@Path("id") int id);

    @GET("media/{id}")
    Call<JsonObject> media_get(@Path("id") int id);

}