package com.madarbash.activity;

import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LevelListDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.madarbash.ApiInterface;
import com.madarbash.MadarbashApplication;
import com.madarbash.R;
import com.madarbash.model.Post;
import com.r0adkll.slidr.Slidr;
import com.r0adkll.slidr.model.SlidrConfig;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class PostViewActivity extends AppCompatActivity {

    public Activity activity;
    public SwipeRefreshLayout swipeRefreshLayout;

    private Toolbar toolbar;

    private Post post;
    public int post_id;
    public int i=-1;
    public int img_i=0;
    public int start = 0;
    public LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

    private LinearLayout content_holder;
    public TextView textView;
    private ImageView image;

    private TextView title;
    private TextView excerpt;
    private ImageView featured_image;
    private TextView date;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_view);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Drawable newIcon = getResources().getDrawable(R.drawable.ic_arrow_forward_black_36dp);
        if (newIcon != null) {
            newIcon.mutate().setColorFilter(Color.parseColor("#ffffff"), PorterDuff.Mode.SRC_IN);
            toolbar.setNavigationIcon(newIcon);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.nothing, R.anim.slide_out);
            }
        });
        toolbar.setTitle("در حال بارگزاری ...");
        SlidrConfig config = new SlidrConfig.Builder()
                .sensitivity(.1f)
                .velocityThreshold(5000f)
                .build();
        Slidr.attach(this,config);

        activity = this;
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);
        content_holder = (LinearLayout) findViewById(R.id.content_holder);

        featured_image = (ImageView) findViewById(R.id.image);
        title = (TextView) findViewById(R.id.title);
        excerpt = (TextView) findViewById(R.id.excerpt);
        date = (TextView) findViewById(R.id.date);

        Bundle bundle = getIntent().getExtras();
        post_id = bundle.getInt("id");
        load_post(post_id);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                load_post(post_id);
            }
        });
    }

    private void load_post(int id){

        swipeRefreshLayout.setRefreshing(true);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(((MadarbashApplication) activity.getApplication()).base_url).addConverterFactory(GsonConverterFactory.create()).build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<JsonObject> call = service.post_get(id);
        call.enqueue(new retrofit2.Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call1, retrofit2.Response<JsonObject> r) {
                if(r.isSuccessful()) {
                    post = new Post(r.body());

                    title.setText(post.getTitle());
                    toolbar.setTitle("");
                    toolbar.setSubtitle(post.getTitle());
                    toolbar.setSubtitleTextColor(Color.parseColor("#ffffff"));
                    //excerpt.setText(post.getContent());

                    //excerpt.setText(Html.fromHtml(post.getRawContent()));
                    set_post_images();

                    date.setText(post.getAgoDate());

                    if (post.getFeaturedMedia() > 0) {
                        Retrofit retrofit = new Retrofit.Builder().baseUrl(((MadarbashApplication) activity.getApplication()).base_url).addConverterFactory(GsonConverterFactory.create()).build();
                        ApiInterface service = retrofit.create(ApiInterface.class);
                        Call<JsonObject> call = service.media_get(post.getFeaturedMedia());
                        call.enqueue(new retrofit2.Callback<JsonObject>() {
                            @Override
                            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> r) {
                                if(r.isSuccessful()) {
                                    Picasso.with(activity).load(r.body().get("guid").getAsJsonObject().get("rendered").getAsString()).into(featured_image);
                                }
                            }
                            @Override
                            public void onFailure(Call<JsonObject> call, Throwable t) {}
                        });
                    }else
                        featured_image.setVisibility(View.GONE);
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.nothing, R.anim.slide_out);
    }

    public void set_post_images(){
        content_holder.removeAllViews();
        i=-1;
        img_i=0;
        start = 0;
        List<String> images = new ArrayList<>();
        String spanned = post.getContent();
        Log.e("jghjhgj",spanned);
        Document doc = Jsoup.parse(post.getContent());
        int img_count = doc.getElementsByTag("img").size();
        for (int i=0; i<img_count; i++)
            images.add(doc.getElementsByTag("img").get(i).attr("src"));

        Log.e("images" , images.toString());

        ArrayList<Integer> breaking_index = new ArrayList<Integer>();
        ArrayList<Integer> length_index = new ArrayList<Integer>();

        int index = spanned.indexOf("<img ");
        while (index >= 0) {
            breaking_index.add(index);
            length_index.add(spanned.indexOf("/>",index)+2-index);
            index = spanned.indexOf("<img ", index+(spanned.indexOf("/>",index)+2-index));
        }

        Log.e("breaking_index" , breaking_index.toString());

        Collections.sort(breaking_index);

        do {
            i++;
            textView = new TextView(activity);
            textView.setTextIsSelectable(true);
            textView.setTypeface(Typeface.createFromAsset(activity.getAssets(), "fonts/IRANSansMobile.ttf"));
            textView.setTextSize(16);

            if (breaking_index.isEmpty() || i == breaking_index.size())
                textView.setText(Html.fromHtml(spanned.substring(start)).toString());
            else {
                textView.setText(Html.fromHtml(spanned.substring(start, Math.min(breaking_index.get(i), spanned.length()))).toString());
                start = breaking_index.get(i) + length_index.get(i);
            }

            textView.setTextColor(Color.parseColor("#333333"));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1)
                textView.setTextDirection(View.TEXT_DIRECTION_FIRST_STRONG);
            textView.setLineSpacing(1f,1.3f);
            content_holder.addView(textView);

            if (i < breaking_index.size() && breaking_index.get(i) != null && images != null && images.size() > img_i && images.get(img_i) != null) {
                final String url = images.get(img_i++);
                image = new ImageView(activity);
                lp1.setMargins(0, 5, 0, 10);
                lp1.gravity = Gravity.FILL_HORIZONTAL;
                image.setLayoutParams(lp1);
                image.setAdjustViewBounds(true);
                Picasso.with(activity).load(url).memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE).networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE).into(image);
                image.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                        builder
                                .setTitle("این تصویر را دانلود می کنید؟")
                                .setMessage("با انتخاب گزینه دانلود ، این تصویر در دستگاه شما دانلود می شود.")
                                .setPositiveButton("دانلود تصویر", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
                                        request.setTitle("دانلود تصویر: " + post.getTitle());
                                        request.setDescription("دانلود تصویر از مطلب");
                                        request.allowScanningByMediaScanner();
                                        File f = new File(url);
                                        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                                        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, f.getName());
                                        // get download service and enqueue file
                                        DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
                                        manager.enqueue(request);
                                    }
                                })
                                .setNegativeButton("انصراف", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });

                        AlertDialog dialog = builder.create();
                        dialog.show();
                        return true;
                    }
                });
                content_holder.addView(image);
                Log.e("hjghjghj",url);
            }
        } while (i < breaking_index.size());

        textView = null;
        image = null;
    }
}
